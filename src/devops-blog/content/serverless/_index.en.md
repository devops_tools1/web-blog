---
title: "Herramientas para serverless"
date: 2021-07-30T11:02:05+06:00
icon: "ti-server"
description: "Herramientas para crear infraestructuras serverless"
type : "docs"
weight: 5
---

## ¿Qué es Serverless?

<p style='text-align: justify;'>
Serverless es una de las nuevas tendencias en la nube. El concepto de serverless se refiere a que la responsabilidad del manejo de la infraestructura es delegada al proveedor de servicio, mientras que el código de la aplicación es totalmente la responsabilidad de el desarrollador. Serverless computing es basada en un arquitectura en donde el proveedor de servicios de la nube es el encargado de ofrecer los servicios y plataformas necesarias para que el código sea ejecutado, sin la preocupación de pensar en la administración de servidores físicos o virtuales. 
</p>

### Ventajas

* Escalabilidad: El proveedor de servicios se encarga del manejo de la escalabilidad de las funciones dependiendo de la demanda.

* Disponibilidad: La disponibilidad es parte de la responsabilidad de el proveedor de servicios

* Disminuir costos: pagas por los recursos que consumes.

![image alt text](/serverless.png)