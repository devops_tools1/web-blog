---
title: "Serverless Framework"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "framework", "serverless"]
---

## ¿Qué es Serverless Framework?

<p style='text-align: justify;'> 
El serverless framework es una herramienta utilizada para la creación de recursos para desplegar funciones e infraestructura que requieren para poder funcionar. Este marco de trabajo provee una herramienta de línea de comandos para poder desplegar y crear la infraestructura necesaria. Es una herramienta open source que actualmente soporta desplieuges en la nube de AWS. 
</p>

![image alt text](/sls-fw.png)

### Conceptos de Serverless Framework

* Funciones: Esta es la unidad de uso de serverless y representa código que se ejecutará en la nube. Es parte de las buenas prácticas de serverless poder separar cada una de las acciones en diferentes funciones. 

* Eventos: como el nombre lo dice, se refiere a los eventos que van a trigerizar la ejecución de una función. Estos eventos pueden crearse a partir de diferentes funciones.

* Recursos: estos son recursos de AWS que pueden crearse como parte de la infraestructura que soportará las funciones de serverless.

* Servicios: en el ambiente del framework se refiere a una unidad que permite organizar funciones y recursos, estos se pueden describir en JSON o YAML

## Documentación de referencia

* [Intro Serverless Framework](https://www.serverless.com/framework/docs/providers/aws/guide/intro)