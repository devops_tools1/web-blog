---
title: "Ejemplo 2: Serverless Framework y uso de Variables"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Ejemplos", "AWS", "Serverless"]
---

### Ejemplo 2

<p style='text-align: justify;'>
Muestra del uso de variables, despliegue de diferentes ambientes e uso de la bandera "stage" para poder realizar despligues en diferentes ambientes.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/dgdva7X0En0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>