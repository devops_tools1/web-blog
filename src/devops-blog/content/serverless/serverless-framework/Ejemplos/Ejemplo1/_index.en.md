---
title: "Ejemplo 1: Primer Proyecto Serverless"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Ejemplos", "AWS", "Serverless"]
---

### Ejemplo 1

<p style='text-align: justify;'>
Creación de un proyecto de serverless framework y muestra de los comandos básicos de Serverless framework
</p>


### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/geN6Kk6H0dU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>