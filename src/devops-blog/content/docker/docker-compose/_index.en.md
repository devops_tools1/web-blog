---
title: "docker-compose"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "contenedores", "Docker"]
---

## ¿Qué es docker-compose?

<p style='text-align: justify;'> 
Esta herramienta es utilizada para poder definir aplicaciones multicontenedores. Esta herramienta utiliza YAML para poder definir cada uno de los contenedores que se utilzarán para poder definir la aplicación. Esta herramienta tiene su propia linea de comandos utilizada para poder levantar los contendores y manejarlos.
</p>

![image alt text](/docker-compose.jpg)

### Casos de uso de Docker Compose

* Ambientes de desarrollo: Se pueden crear ambientes de desarrollo en donde la aplicación puede ser probada de forma aislada, esta aplicación puede contener diferentes componentes como base de datos, redis u otros servicios que pueden definirse con esta herramienta.

* Ambientes de pruebas: Puede utilizarse en herramientas de CI/CD para levantar un ambiente de pruebas efímero.

* Despliegues en un host único: Puede utilizarse para desplegar una aplicación en un solo host, en donde todos los servicios estarán disponibles en un solo servidor.

## Documentación de referencia

* [docker-compose documentacion](https://docs.docker.com/compose/)
