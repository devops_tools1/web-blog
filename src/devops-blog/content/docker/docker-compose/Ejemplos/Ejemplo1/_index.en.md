---
title: "Ejemplo 1: Comandos para docker-compose"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Ejemplos", "Docker"]
---

### Ejemplo 1

<p style='text-align: justify;'>
Este primer ejemplo mostrará la forma de utilizar los comandos de docker-compose para poder interactuar con la herramienta.
</p>


### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nevkxh8NErg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>