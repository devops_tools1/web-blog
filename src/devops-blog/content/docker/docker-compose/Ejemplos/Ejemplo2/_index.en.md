---
title: "Ejemplo 2: Creando una aplicación con docker-compose"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Containers", "Docker", "Dockerfile"]
---

### Ejemplo 2

<p style='text-align: justify;'>
En este ejemplo utilizaremos docker-compose para poder crear una aplicación en un ambiente de pruebas local.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/7R5eZLKTCJA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>