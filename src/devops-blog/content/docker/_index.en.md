---
title: "Herramientas para el uso de contenedores"
date: 2021-07-30T11:02:05+06:00
icon: "ti-package"
description: "Herramientas para el uso de contenedores"
type : "docs"
weight: 1
---

## Uso de contenedores

<p style='text-align: justify;'>
El uso de contenedores se ha popularizado durante los últimos años, estos se han utilizado para poder aislar las dependencias de una aplicación haciendo que este pueda funcionar en diferentes ambientes de una forma consistente. Los contenedores son efímeros por lo que los datos en el contenedor no persisten a menos que se le indique al contenedor que los datos deben de persistir.
</p>

### Máquinas virtuales y Contenedores

<p style='text-align: justify;'>
Una tecnología similar a los contenedores son las máquinas virtuales, ambas tienen la capacidad de poder encapsular aplicaciones pero tienen diferencias importantes que pueden definir el caso de uso entre una y otra. 
</p>

#### Máquinas Virtuales

<p style='text-align: justify;'>
Las máquinas virtuales son administradas por una herramienta que se le denomina hipervisor. El hipervisor permite crear múltiples máquinas virtuales en un sistema anfitrión, también permite la administración del hardware en las diferentes máquinas virtuales y por supuesto cada uno de los sistemas se encuentra en total aislamiento uno del otro. Proveedores de nube ofrecen crear máquinas virtuales con diferentes configuraciones de acuerdo a necesidades, es una de las elecciones preferidas por empresas para poder desplegar aplicaciones utilizando servidores virtuales.
</p>

#### Contenedores

<p style='text-align: justify;'>
Los últimos años se ha popularizado el uso de contenedores para el desarrollo de aplicaciones, una de estas herramientas es Docker. Este es un proyecto de código abierto utilizado para la automatización y manejo de contenedores. Cuando se hace mención del término “contenedor” se hace referencia a una forma de virtualización mucho más ligera que permite crear un ambiente aislado donde se pueden empaquetar las librerías, dependencias y otro tipo de herramientas necesarias para una aplicación. En el caso de contenedores de Docker, se utiliza un motor para poder construir, guardar y correr contenedores, el cual corre a nivel del kernel del sistema operativo anfitrión. El uso de los contenedores ha permitido la flexibilidad en el despliegue y empaquetado de aplicaciones, es muy utilizado como parte de las buenas prácticas DevOps.
</p>


![image alt text](/vm-container.png)