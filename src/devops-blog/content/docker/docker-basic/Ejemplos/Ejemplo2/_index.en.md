---
title: "Ejemplo 2: Dockerfile"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Containers", "Docker", "Dockerfile"]
---

### Ejemplo 2

<p style='text-align: justify;'>
En este ejemplo veremos la forma de crear un Dockerfile, y los comandos utilizados para poder construir una imagen.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/OfIGouTlp9Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>