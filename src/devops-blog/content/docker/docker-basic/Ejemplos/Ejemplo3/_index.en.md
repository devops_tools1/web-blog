---
title: "Ejemplo 3: Registro de imágenes"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Containers", "Docker", "Dockerfile"]
---

### Ejemplo 3

<p style='text-align: justify;'>
Este ejemplo mostrará la forma de construir una imagen y guardarlo en un registro, en este ejemplo utilizaremos Dockerhub.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q0PaftPQd-c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>