---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Ejemplos", "Docker", "Contenedores"]
---

## Antes de empezar

<p style='text-align: justify;'>
Para empezar debemos de tener las siguientes herramientas instaladas para que todo el ambiente de pruebas pueda ejecutarse satisfactoriamente:
</p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

* [Git Instalación](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. Clonar el repositorio de ejemplos:

```bash
$ git clone https://gitlab.com/devops_tools1/containers-docker.git
```


2. Comprobar que podamos correr los comandos de Docker

```bash
$ docker --version
Docker version 20.10.9, build c2ea9bc
```

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/UVRG-ESzOC4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>