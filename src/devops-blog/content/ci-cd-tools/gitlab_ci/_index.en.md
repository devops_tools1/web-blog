---
title: "Gitlab CI"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 3
draft: false
# search related keywords
keywords: ["iac", "aws", "CI/CD", "Gitlab"]
---

## ¿Qué es Gitlab CI?

<p style='text-align: justify;'> 
Gitlab es una de las plataformas utilizadas por equipos de desarrolladores para poder llevar el versionamiento de su código e implementar diferentes procesos de automatización para el desarrollo y manejo de proyectos. Actualmente podemos encontrar la plataforma de Gitlab que nos permite crear y manejar repositorios de código y otras funcionalidades, pero existe la posibilidad de implementar un servidor de gitlab sobre una infraestructura propia. Existen dos versiones disponibles de Gitlab: la primera es la versión para empresas y la segunda es la versión para la comunidad, el código de Gitlab está disponible bajo la licencia MIT y es una plataforma de código abierto.
</p>

![image alt text](/gitlab-ci.png)

### Ventajas

<p style='text-align: justify;'> 
Ventajas de usar gitlab:
</p>

* La administración y configuración es sencilla de utilizar
* Integración de prácticas para el ciclo DevOps
* Automatizar tareas para el despliegue de aplicaciones
* Manejo de repositorios de Git
* Puede realizarse una implementación con recursos propios
* Pueden utilizarse agentes externos que puedan ejecutar tareas
* Manejo de permisos de los diferentes colaboradores
* Permite poder importar y exportar proyectos de otras plataformas

### Uso

<p style='text-align: justify;'> 
Para iniciar a integrar el uso de la funcionalidad de gitlab, podemos crear un repositorio en gitlab. En una cuenta grátis podemos tener 400 minutos de CI/CD gratis, para iniciar la prueba es bastante útil. Para inicar el uso, gitlab por defecto hace uso de un archivo de configuración que se debe encontra en la raíz del repositorio. El archivo debe llamarse .gitlab-ci.yml
</p>

Un ejemplo del archivo:

```yaml
variables:
  # definicion de variables de ambiente

stages:
  - build
  - deploy

build:
  only:
    refs:
      - main
  stage: build
  script:
    # comandos de build

deploy:
  only:
    refs:
      - main
  stage: deploy
  script:
    # comandos para desplegar
```

#### Acciones de gitlab

<p style='text-align: justify;'> 
El ejemplo anterior muestra una configuración básica del archivo que gitlab utiliza para poder construir, probar y desplegar la aplicación. Podemos especificar las acciones que se deben haciendo referencia a que se ejecute cuando un cambio ocurra en una rama específica o una etiqueta de git. Pueden haber acciones que se ejecuten en ramas especificas.
</p>

* [Más sobre el uso del archivo YAML](https://docs.gitlab.com/ee/ci/quick_start/)

#### Variables de Ambiente
<p style='text-align: justify;'> 
Gitlab permite definir variables en el archivo o se puede incluir en la configuración del proyecto, esto es muy útil en caso se tengan credenciales que no se pueden exponer en el archivo de configuración.
</p>

* [Mas sobre el uso de variables](https://docs.gitlab.com/ee/ci/variables/)