---
title: "Ejemplo 2: Utilizando variables de ambiente"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Jenkins", "Ejemplos", "CI/CD"]
---

### Ejemplo 2

<p style='text-align: justify;'> En este ejemplo vemos el uso de variables de ambiente y completamos el ejemplo para crear una imagen de docker </p>

### Demo 

<iframe width="560" height="315" src="https://www.youtube.com/embed/xfLkkEWba_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>