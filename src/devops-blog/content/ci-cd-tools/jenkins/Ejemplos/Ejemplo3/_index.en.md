---
title: "Ejemplo 3: Jenkins Configuración como código"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 3
draft: false
# search related keywords
keywords: ["Docker", "Jenkins", "Ejemplos", "CI/CD"]
---

### Ejemplo 3

<p style='text-align: justify;'>
Este ejemplo mostrará el uso de un plugin muy importante para configurar el servidor de Jenkins utilizando código.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/vgu3b_h-2ac" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>