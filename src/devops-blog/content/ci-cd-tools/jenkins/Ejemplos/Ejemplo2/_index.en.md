---
title: "Ejemplo 2: Creando Jobs de Jenkins"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Jenkins", "Ejemplos", "CI/CD"]
---

### Ejemplo 2

<p style='text-align: justify;'>
Este segundo ejemplo busca mostrar como crear y configurar un Job de jenkins para automatizar pasos, este mostrará como poder crear el job por medio de la interfaz gráfica y usando el lenguaje de definición de Groovy.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/9Y2wD0k66k8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>