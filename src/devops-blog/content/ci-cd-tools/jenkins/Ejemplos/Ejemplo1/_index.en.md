---
title: "Ejemplo 1: Introducción a Jenkins"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["Docker", "Jenkins", "Ejemplos", "CI/CD"]
---

### Ejemplo 1

<p style='text-align: justify;'>
Este primer ejemplo contempla la explicación de como funciona Jenkins, como podemos navegar en la interfaz gráfica, el uso de los usuarios, plugins y otras configuraciones.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/dffPzdHwhWk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>