---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Jenkins","Ejemplos", "Docker"]
---

## Antes de empezar

<p style='text-align: justify;'> Para poder probar los ejemplos de Jenkins que se explicarán en esta sección es necesario prepara el ambiente ya que se provee una forma rápida de tener un servidor Jenkins utilizando Docker. Se necesita tener las siguientes herramientas instaladas para probar cada uno de los ejemplos: </p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

* [Instalación de Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

* [Instalación de Docker Compose](https://docs.docker.com/compose/install/)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. Preparando las variables de entorn, para ello hay que crear un archivo en el repositorio llamado: **.env** este archivo debe contener las variables de entorno utilizadas por docker-compose para configurar el servidor.

```bash
ADMIN_USER=<VALOR_USUARIO_ADMIN>
ADMIN_PASSWORD=<VALOR_PASSWORD_ADMIN>
LOCAL=<VALOR_USUARIO_2>
LOCAL_PASSWORD=<VALOR_PASSWORD>
BASE_REPO=https://gitlab.com/devops_tools1/cicd_jenkins.git # REPOSITORIO QUE TIENE LA CONFIGURACIÓN DE JENKINS, SE RECOMIENDA DEJAR ESTE REPOSITORIO
RAMA_BASE=<RAMA_REPO_BASE> # RAMA BASE QUE SE UTILIZARÁ DEL REPOSITORIO ANTERIOR
EXECUTORS=10 # NUMERO DE EJECUTORES QUE ESTARAN DISPONIBLES EN JENKINS
```

2. clonar el repositorio de ejemplos:

```bash
$ git clone https://gitlab.com/devops_tools1/cicd_jenkins.git
```

3. ingresar al directorio del repositorio:

```bash
$ cd cicd_jenkins
```

4. comprobar que podemos construir la imagen:

```bash
$ docker-compose build
```

5. comprobar que podemos correr la imagen y montar el volumen para poder empezar a probar:

```bash
$ docker-compose up -d 
```

Los pasos anteriores levantarán el servidor Jenkins disponible en el puerto **8080**

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/PyG3hgNRDik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>