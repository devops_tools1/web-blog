---
title: "Jenkins Server"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "CI/CD", "Jenkins"]
---

## ¿Qué es Jenkins Server?

<p style='text-align: justify;'> 
Este servidor de automatización es uno de los más utilizados por empresas ya que es muy versátil y puede adaptarse a las necesidades que se desean. Una de las características que hacen esta herramienta muy importante, es el hecho que es de código abierto y permite implementar estrategías DevOps para construir y desplegar aplicaciones de forma continua.
</p>

![image alt text](/jenkinshero.jpg)

<p style='text-align: justify;'> 
Este servidor puede implementarse en diferentes ambientes, según sea la necesidad. Una de las implementaciones más comunes es el uso de recursos en la nube para crear el servidor y poder escalar según sea las necesidades. Algunas de las funcionalidades que hacen a Jenkins un servidor muy utilizado son las siguientes
</p>

### Ventajas de Jenkins

* Instalable en diferentes sistemas operativos
* Actualizaciones constantes y fáciles de realizar
* Las funcionalidades pueden ser agregadas utilizando extensiones al servidor
* Totalmente configurable según las necesidades
* Utiliza el concepto de arquitectura llamada maestro y esclavo, lo que permite distribuir el trabajo en diferentes nodos.
* Integración con notificaciones
* Construido en Java
* La implementación de la configuración puede realizarse en el lenguaje de programación Groovy.
* Es gratuito, lo único costo que se debe de completar es el de la infraestructura y recursos utilizados para soportar las funcionalidades del servidor.
