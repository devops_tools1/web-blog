---
title: "Herramientas de CI/CD"
date: 2021-10-02T11:02:05+06:00
icon: "ti-infinite"
description: "Herramientas para automatizar la integración y despliegue de aplicaciones"
type : "docs"
weight: 3
---

## ¿Qué son?

<p style='text-align: justify;'>
Parte importante de el desarrollo de una aplicación es poder constantemente desplegando cambios y asegurando que los cambios en el código sean integrados y probados constantemente. Las herramientas de CI/CD son utilizadas para poder automatizar cada uno de los pasos que componen la construcción de una aplicación, es importante el utilizar un sistema de control de versiones que permite llevar un historial de versiones previas, contribuir en equipo y minimizar riesgos de inconsistencia en el código.
</p>

### Conceptos relacionadas a CI/CD

* Despliegue de código: Este proceso es importante ya que involucra, la planificación, mantenimiento y despliegue de una nueva versión de la aplicación. Este aspecto es importante ya que esta fuertemente relacionado con el la cálidad del software.

* Integración Continua (CI): la integración continua es un aspecto importante a considerar, este proceso involucra integrar los cambios que se van agregando al código. Este proceso tiene como objetivo evitar cualquier tipo de problemas al integrar el código. En lugar de integrar grandes cantidades de código, se integran pequeños cambios que van aportando al código, este proceso incluye las pruebas del mismo para asegurar que la aplicación presente problemas.

* Entrega Continua y Despliegue Continuo (CD): Estos términos en inglés se conocen como **Continuous Delivery** y **Continuous Deployment**. Ámbos términos son confundidos entre sí pero describen diferentes partes del proceso. **Continuous Delivery** describe la capacidad de poder generar artefactos constantemente que estén listos para ser desplegados mientras que **Continuous Deployment** a la capacidad de desplegar artefactos o nuevas versiones de la aplicación.

#### Herramientas de CI/CD

<p style='text-align: justify;'>Existen algunas herramientas que permiten automatizar cada uno de los procesos del desarrollo de código. Las herramientas de las cuáles se presentarán los ejemplos son las siguientes</p>

* Gitlab CI

![image alt text](/gitlab_ci_cd.png)

* Guthub Actions

![image alt text](/github_actions.png)

* Jenkins

![image alt text](/jenkins.png)
