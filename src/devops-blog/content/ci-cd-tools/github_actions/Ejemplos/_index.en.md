---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Jenkins","Ejemplos", "Docker"]
---

## Antes de empezar

<p style='text-align: justify;'>
Antes de iniciar necesitaremos un repositorio de github, este repositorio es público por lo que se podrá tener acceso y ver el código del mismo. Herramientas que necesitamos para el siguiente ejemplo son los siguientes:
</p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

* [Git Instalación](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

* [Repositorio de Github de ejemplos](https://github.com/ricardocutzhernandez/cicd_github_actions)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. clonar el repositorio de ejemplos:

```bash
$ git clone https://github.com/ricardocutzhernandez/cicd_github_actions.git
```

Ya que el correr los ejemplos se realizará utilizando gitlab, solo se necesita tener acceso al código.

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/JMj4LYXRfII" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>