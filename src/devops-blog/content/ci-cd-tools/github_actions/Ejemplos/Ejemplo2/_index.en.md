---
title: "Ejemplo 2: Utilizando variables de ambiente"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Jenkins", "Ejemplos", "CI/CD"]
---

### Ejemplo 2

<p style='text-align: justify;'>
Este segundo ejemplo utilizaremos variables de ambiente y como se configuran en un repositorio de github.
</p>

### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/hzqoaYpQ8PI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>