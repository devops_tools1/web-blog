---
title: "Ejemplo 1: Configurando el repositorio"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Docker", "Github", "Ejemplos", "CI/CD"]
---

### Ejemplo 1

<p style='text-align: justify;'>
En este primer ejemplo de github actios veremos como configurar el repositorio y empezar a utilizar github actions
</p>


### Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/oYDuoWPV7nw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>