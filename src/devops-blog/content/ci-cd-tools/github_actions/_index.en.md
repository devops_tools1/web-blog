---
title: "Github Actions"
date: 2021-10-12T11:02:05+06:00
lastmod: 2021-10-12T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "CI/CD", "Github"]
---

## ¿Qué es Github Actions?

<p style='text-align: justify;'> 
Además del almacenamiento de repositorios, Github ofrece más funcionalidades que lo hacen una herramienta ideal para la colaboración y automatización de tareas. Una de estas funcionalidades, la cual es muy reciente, se llama Github Actions. Esta funcionalidad permite que se puedan ejecutar tareas automatizadas según la acción o el evento que se esté realizando en el repositorio.
</p>

![image alt text](/github_actions.png)

<p style='text-align: justify;'> 
Las acciones automatizadas se pueden definir siguiendo los pasos a continuación:
</p>

* Cada repositorio debe de tener una carpeta que define diferentes “workflows” o flujos de trabajo. Cada flujo de trabajo contiene pasos que se deben de realizar para completar un determinado objetivo.

* Un flujo de trabajo puede ejecutarse de acuerdo a diferentes eventos. Los eventos son acciones que se realizan en el repositorio, como por ejemplo: cuando alguien agrega código a un repositorio puede ejecutarse una acción que genere algún tipo de artefacto o pruebas.

* Cada uno de los flujos de trabajo contienen diferentes “jobs” o trabajos que describen una serie de pasos que ejecutan acciones. Estos trabajos son las definiciones de cada una de las acciones que se deben de realizar al momento de que un evento en específico sea invocado.

### Uso de github actions

<p style='text-align: justify;'> 
Son tareas que permiten automatizar las tareas que componen el cíclo de vida de una aplicación.
</p>

Los componentes de github actions:

*  Workflows: describe un conjunto de "job" que seran ejecutados cuando un evento suceda.
*  Events: Los eventos que inician la ejecución del workflow. Puede ser un push, pull request, etc.

*  Jobs: Contiene una serie de pasos que se ejecutan, estos pueden ejecutarse en paralelo, pueden ser secuenciales.

*  Actions: Son comandos que pueden ejecutarse e incluirse en un job.


Ejemplo: iniciar el uso con workflow:

* En el repositorio se deben crear los siguientes directorios **.github/workflows/**
* En este directorio podemos crear el un archivo YAML que permita agregar las tareas que se ejecutaran por medio de gtihub actions.

```yaml
name: ejemplo

on:
  push:  # eventos
    branches: [master]

jobs:
  build:
    runs-on: ubuntu-latest
    env:
      # variables de ambiente
    steps:
      # comandos a ejecutar
  deploy:
    runs-on: ubuntu-latest
    needs: [build]
    steps:
      # comandos a ejecutar
```

* En el ejemplo anterior podemos observar las siguientes partes del archivo.

  * Este es opcional y permite colocarle nombre al **workflow** que se ejecutará

  ```yaml
  name: ejemplo
  ```

  * La siguiente porción define el evento que va a iniciar las acciones, en este caso el evento será un **push** en la rama **master** 

  ```yaml
  on:
    push:
      branches: [master]
  ```

  * La definición de los **jobs** permite definir cada una de las tareas que se ejecutarán como parte de la automatizaciones.

  ```yaml
  jobs:
    build:
      # definición de las acciones de build
    deploy:
      # definición de las acciones de deploy
  ```

  ### Documentación de referencia

  * [Documentación de Github Actions](https://docs.github.com/en/actions/learn-github-actions/understanding-github-actions)