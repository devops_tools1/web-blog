---
title: "Herramientas de Infraestructura como Código"
date: 2021-07-30T11:02:05+06:00
icon: "ti-files"
description: "Ejemplos de herramientas para provisionar infraestructura utilizando código"
type : "docs"
weight: 2
---

## ¿Qué son?

<p style='text-align: justify;'> Parte de las buenas prácticas en el ámbito DevOps es poder implementar automatización en los procesos, esto con el fin de evitar errores humanos y agilizar las tareas. Las herramientas de infraestructura como código que permiten definir recursos son utilizadas para crear consistencia en la estructura de un sistema, incluso poder definir y documentar todas las piezas que forman parte del mismo. En la industria, el uso de este tipo de herramientas se ha convertido en algo esencial. Empresas como Google, Amazon, Netflix y muchas otras, que manejan un flujo de clientes bastante alto, requieren escalar y manejar su infraestructura de forma automatizada, repetible y consistente. Es importante aclarar que las herramientas de infraestructura como código no solo aplican a recursos en la nube, sino que pueden ser aplicados a diferentes tipos de ambientes. </p>

### ¿Qué ventajas puedo obtener?

El uso de este tipo de herramientas trae consigo ciertas ventajas que podemos observar:
* Consistencia en la configuración de recursos.
* Crear ambientes que puedan ser reproducibles.
* Automatización en el proceso de aprovisionamiento de infraestructura
* Control de las versiones y cambios de la infraestructura
* Opción para crear planes de recuperación en caso de desastres

#### Puntos a considerar

<p style='text-align: justify;'>Es cierto que los beneficios de usar este tipo de herramientas son varios pero es importante tomar en cuenta que esto implica retos que son necesarios mencionar. </p>

* Manejar cambios en las configuraciones: existen ocasiones en que por alguna razón se realizan cambios manuales a la configuración de un recurso, lo que introduce inconsistencia en lo existe definido en el código.
* Duplicación de errores: en caso de que parte de la definición de la infraestructura incluya errores, este puede verse duplicado en los ambientes en donde la configuración se aplicó, por lo que realizar pruebas es necesario para mitigar este tipo de errores.
* Conocimientos y destrezas: introducir este tipo de herramientas incluye una curva de aprendizaje, y esto puede tomar tiempo para los equipos.

<p style='text-align: justify;'>Existen diferentes herramientas que son utilizadas en la industria, a continuación se hará una mención de algunas de ellas incluyendo ejemplos de su utilización en el ámbito DevOps.</p>

### ¿Cómo se aplica en escenario real?

<p style='text-align: justify;'>
Para ejemplicar un caso de la vida real podemos plantear el siguiente requerimiento: se necesita diseñar la infraestructura de una aplicación web que pueda escalar de acuerdo a la demanda, para esta aplicación debemos de tener tres ambientes diferentes los cuales son: desarrollo, pruebas y producción y se utilizará AWS como el proveedor donde se tendrá la solución. Para la solución del caso podriamos plantear una arquitectura que permita tener la aplicación detrás de un balanceador de carga y un servidor de base de datos pero para el soporte de los diferentes ambientes que se requieren podriamos pensar realizar las misma configuración manualmente para cada ambiente, pero esto no es buena idea ya que la inconsistencia en la configuración es un riesgo que puede suceder por lo que podemos utilizar una herramienta que nos permita declarar la infraestructura necesaria y poder parametrizarla para tener una configuración consistente en los tres ambientes necesarios.
</p>
