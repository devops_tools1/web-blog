---
title: "Cloudformation"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "Cloudformation"]
---

## ¿Qué es Cloudformation?

<p style='text-align: justify;'> Es una herramienta de infraestructura como código utilizada exclusivamente para definir y provisionar recursos en la nube de AWS. En el ámbito de implementación de proyectos de automatización en la nube de AWS, Cloudformation es una de las herramientas más ampliamente utilizadas. </p>

<p style='text-align: justify;'>
Esta herramienta de AWS permite definir los recursos necesarios utilizando un lenguaje llamado YAML, el cuál es utilizado comúnmente para definir configuraciones. Existe una amplia documentación de cada uno de los recursos y servicios que AWS ofrece. Algunos de los conceptos que se manejan al utilizar la herramienta de Cloudformation son los siguientes:
</p>

* Las plantillas (templates): se refiere al archivo que define la configuración de cada uno de los recursos que se desean de crear.
* Las pilas de recursos (stacks): este concepto se refiere el tener un conjunto de recursos definidos en uno o más plantillas, estos se pueden crear, actualizar e incluso eliminar.
* Conjunto de cambios (change sets): se refiere a la planificación de cambios que se desean realizar al actualizar una pila de recursos, el servicio de Cloudformation en AWS permite visualizar las acciones que se realizará al momento de querer actualizar los recursos, esto con el objetivo de realizar una revisión de las acciones a ejecutar.

### Beneficios del uso de Cloudformation

Utilizar Cloudformation para poder manejar los recursos podemos tener los siguientes beneficios:

* Implementación de infraestructura como código: todos los recursos son definidos utilizando código, en este caso utilizando un YAML. Este código puede ser almacenado en un repositorio para el manejo del control.

* Uso de etiquetas: podemos manejar el etiquetado de recursos para poder identificarlos dependiendo los recursos que estamos utilizando.

* Creación y eliminación de recursos de forma dinámica: podemos implementar automatizaciones para poder remover o agregar recursos de ser necesario.

* Creación de diferentes "Stacks": podemos separar nuestros recursos por medio de "Stacks" que pueden definit diferentes partes de nuestra infraestructura, como por ejemplo: recursos de red, bases de datos, servidores, etc.

* Soporte de la mayoria de recursos de AWS: La mayoría de servicios y recursos en AWS son soportados por cloudformation.