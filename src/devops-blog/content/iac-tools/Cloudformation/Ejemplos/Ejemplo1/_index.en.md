---
title: "Ejemplo 1: Bucket de S3 con Cloudformation"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Cloudformation","Ejemplos"]
---

### Ejemplo 1

<p style='text-align: justify;'> Los ejemplos buscan dar una introducción a lo que es en realidad cloudformation, y alguna de sus características y porque se utiliza en AWS </p>

### Creando un bucket de S3

<p style='text-align: justify;'>
El siguiente ejemplo tiene el propósito mostrar cómo crear recursos utilizando un template de Cloudformation definiendo cada una de las partes que se requieren definir para crear recursos de cloudformation. En el ejemplo veremos lo siguiente:
</p>

* Como crear recursos utilizando un template de cloudformation.
* Crear un bucket de S3.
* Actualizar un template de cloudformation utilizando **Change Sets**

#### Demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7oH2Q6B-apE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>