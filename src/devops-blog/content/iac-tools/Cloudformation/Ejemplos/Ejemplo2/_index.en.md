---
title: "Ejemplo 2: Servidor de nginx con EC2"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura","Cloudformation","Ejemplos"]
---

### Ejemplo 2

### Creando un servidor de NGINX utilizando EC2

<p style='text-align: justify;'> En el siguiente ejemplo veremos como utilizar Cloudformation para poder crear un servidor utilizando el servicio de EC2 de AWS. Crearemos un SecurityGroup donde veremos como utilizar las funciones de Cloudformation para poder referenciar otros recursos dentro del mismo template. El ejemplo muestra lo siguiente:</p>

* Crear una instancia de EC2.
* Crear un **SecurityGroup** para dar acceso al servidor.
* Usar las funciones de Cloudformation: **Ref** y **GetAtt**
* Utilizar **Outputs** en el template de Cloudformation.
* Usar el **UserData** de una instancia para instalar **nginx** y crear una página estática de HTML
* Actualizar el stack de Cloudformation utilizando **ChangeSets**


#### Demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/70N1Agi7upQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>