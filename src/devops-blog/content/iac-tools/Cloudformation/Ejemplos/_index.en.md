---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Cloudformation","Ejemplos", "Docker"]
---

## Antes de empezar

<p style='text-align: justify;'> Para poder probar los ejemplos provistos, es necesario contar con una cuenta de AWS. </p>

* [Creación de una cuenta en AWS](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)

* Git: herramienta de control de versiones, puedes instalarla aqui: [documentacion](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. clonar el repositorio de ejemplos:

```bash
$ git clone https://gitlab.com/devops_tools1/iac-cloudformation.git
```

2. Ingresando al servicio de **Cloudformation** en AWS.

![image alt text](/aws-console.png)


3. Buscar **Cloudformation** en el buscador de AWS

![image alt text](/aws-cf-search.png)

4. Puedes ver todos los **Stacks** en la consola de AWS, para poder empezar a probar los ejemplos.

![image alt text](/aws-cf-stacks.png)

* Ahora estas preparado para poder probar los ejemplos de Cloudformation

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/1qhY8EOrYZ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>