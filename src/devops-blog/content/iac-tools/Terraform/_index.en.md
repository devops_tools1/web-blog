---
title: "Terraform"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "Terraform"]
---

## ¿Qué es Terraform?

<p style='text-align: justify;'> Es una herramienta de código abierto que permite administrar y provisionar infraestructura utilizando un lenguaje declarativo, esta herramienta es desarrollada por una compañía llamada Hashicorp. Terraform es utilizada mayormente como herramienta para provisionar recursos permitiendo la automatización del proceso y puede ser utilizada en conjunto a otras herramientas para cubrir las necesidades que se tienen para la infraestructura y configuración de un aplicación. </p>

<p style='text-align: justify;'>
La herramienta de terraform utiliza su propio lenguaje para definir cada uno de los recursos que se desean crear y existen diferentes proveedores que pueden utilizarse para provisionar recursos. Algunos de los proveedores que tiene terraform son:
</p>

* Oracle Cloud
* AWS
* Google Cloud
* Kubernetes
* Azure

### Ventajas de Terraform

<p style='text-align: justify;'>
Las ventajas que podemos usar:
</p>

* Manejo de la infraestructura como código: la definición de los recursos se manejan utilizando archivos de configuración que permiten manejar los cambios de forma controlada.

* Terraform plan: esta herramientas permiten mostrar las acciones que se van a ejecutar.

* Flexibilidad: La herramienta de Terraform permite usar diferentes funciones que permiten agregar recursos de forma dinámica.


### Lenguaje de Terraform

<p style='text-align: justify;'> Terraform utiliza su propio lenguaje para la definición de recursos, dependiendo del proveedor que se utilizará existen diferentes tipos de definiciones.
</p>

* Ejemplo usando el proveedor para docker:

```
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.15.0"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_container" "ubuntu" {
  name  = "ejemplo_ubuntu"
  image = "ubuntu:20.04"
}
```

* Terraform con AWS

```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

resource "aws_s3_bucket" "ejemplo_b" {
  bucket = "ejemplo_bucket"
  acl    = "private"
}
```