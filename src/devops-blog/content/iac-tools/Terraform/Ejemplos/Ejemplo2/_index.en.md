---
title: "Ejemplo 2: Terraform workspaces y recursos en AWS"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura","Cloudformation","Ejemplos"]
---

### Ejemplo 2

<p style='text-align: justify;'>
En el ejemplo 2 se utilizará la configuración y proveedor de AWS para poder crear recursos en una cuenta de AWS. Se utilizará el recurso de aws_instance para poder crear de forma dinámica servidores. El ejemplo contiene las siguiente información
</p>

* Uso de el comando **workspace**
* Creación de workspaces para manejar múltiples ambientes
* Listar los workspaces
* Cambiar de workspaces
* Crear recursos en la nube de AWS
* Crear recursos en diferentes workspaces
* Manejo de funciones en terraform (lookup, zipmap)
* Creación dinamica de recursos con terraform
* Uso del commando **show** para mostrar el estado de la infraestructura.


### Demo:

#### Ejemplo 2: configuración

<p style='text-align: justify;'>
Explicación del ejemplo 2, inlcuyendo la definición de el objeto que define los servidores que se crearán en el ejemplo (Instancias EC2). Incluye la creación y uso de Workspaces en terraform.
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/K8J_pB9omeQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Ejemplo 2: Depliegue de Servidores en "dev" y "stage" (usando workspaces)

<p style='text-align: justify;'>
Explicación de la creación de un "Mapa" en terraform usando la función zipmap, creación de instancias EC2 en diferentes workspaces. Uso de la función for_each. Uso de la función lookup.
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/_SiZco3vQdo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>