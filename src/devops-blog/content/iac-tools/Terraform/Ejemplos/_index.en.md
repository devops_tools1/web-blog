---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Terraform","Ejemplos", "Docker"]
---

## Antes de empezar

<p style='text-align: justify;'>
Los ejemplos que se presentaran se utilizará una cuenta de AWS para desplegar la infraestructura y crear los recursos. Haremos uso de la herramienta de git para poder clonar el repositorio donde se encuentra los ejemplos a utilizar.
</p>

* [Creación de una cuenta en AWS](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/)

* Git: herramienta de control de versiones, puedes instalarla aqui: [documentacion](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Instalación de Terraform

<p style='text-align: justify;'>
Terraform es una herramienta de línea de comandos que puede ser instalado en diferentes sistemas operativos. En el repositorio de ejemplos se proveerá de una imagen de Docker para poder tener instalado la versión necesaria para probar cada uno de los ejemplos. Si deseas instalarlo en tu ambiente local sin utilizar la imagen de Docker puedes seguir la siguiente documentación:
</p>

* [Instalación de Terraform](https://www.terraform.io/downloads.html)

## Preparando el ambiente:

<p style='text-align: justify;'>
En el repositorio se tiene una definición de un contenedor que puede utilizarse para ejecutar y probar los ejemplos.
</p>

1. Construir la imagen de Docker

```bash
$ docker build -t tf:1.0 .
```

2. Correr la imagen de Docker y montar los volúmenes para poder correr terraform en un contenedor.

```bash
$ docker run -it --rm -v $PWD:/home tf:1.0 bash
```

3. Dentro del contenedor podemos ejecutar los comandos de terraform.

```bash
$ terraform -version
```

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/EHtNlKJKI08" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>