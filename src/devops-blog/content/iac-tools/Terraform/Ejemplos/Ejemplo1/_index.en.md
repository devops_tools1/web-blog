---
title: "Ejemplo 1: Introducción al uso de Terraform"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Terraform","Ejemplos"]
---

### Ejemplo 1

<p style='text-align: justify;'>
En el ejemplo 1 podemos ver una estructura básica de un proyecto de terraform. Se utiliza el recurso de terraform local_file para poder mostrar una funcionalidad básica de terraform. En el ejemplo 1 se tratará lo siguiente:
</p>

* Estructura de un proyecto de terraform (básico)
* Uso del archivo de variables
* Pasar variables a un modulo de terraform utilizando la bandera **-var-file**
* Uso del commando **plan**
* Uso del comando **apply**
* Uso del comando **destroy**


### Demo:

#### Ejemplo 1: base y comándos de terraform

<p style='text-align: justify;'>
Se muestra como está la estructura del ejemplo 1 y los comandos básicos de terraform.
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/9K3E9EGwWnw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Ejemplo 1: Outputs

<p style='text-align: justify;'>
Se muestra como manejar los outputs y el aplicar cambios a la configuración. Uso del commando destroy.
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/hu1K1xOietI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Ejemplo 1: Recursos dinámicos y uso de funcion for_each

<p style='text-align: justify;'>
El ejemplo muestra como crear variables de tipo arreglo, iteración y creación de recursos dinámicos.
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kJsmRb67EB8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>