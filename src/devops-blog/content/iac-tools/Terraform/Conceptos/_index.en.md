---
title: "Conceptos"
date: 2021-07-30T11:02:05+06:00
lastmod: 2021-07-30T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "Terraform"]
---

## Fundamentos

Los conceptos que se manejan en Terraform son:

* Archivos de configuración: Los archivos de configuración contienen la sintaxis de terraform para definir los recursos.
* Archivo de estado: Este archivo de estado permite a terraform conocer cuál es el estado de la configuración de la infraestructura. En caso de que la configuración cambie, terraform se encarga de aplicar las configuraciones necesarias para llegar al estado deseado.
* Proveedores: representa una integración con proveedores de nube u otras tecnologías que permiten la interacción de terraform y el proveedor para manejar los recursos.
* Espacios de trabajo: los espacios de trabajo son divisiones lógicas que la herramienta utiliza para dividir los recursos, un ejemplo común de su uso es cuando se necesita replicar diferentes ambientes (producción, desarrollo)
* Modulos: Terraform puede organizarse en diferentes modulos que son un conjunto de archivos con extensión **.tf**

### Estructura básica de proyecto

<p style='text-align: justify;'> La estructura básica sugerida para un proyecto de terraform es la siguiente: </p>

```
terraform/
  main.tf
  outputs.tf
  variables.tf

  modules/
    modulo_ec2/
      main.tf
      outputs.tf
      variables.tf

    modulo_s3/
      main.tf
      outputs.tf
      variables.tf
```
#### Archivos de terraform

<p style='text-align: justify;'> Para estructurar un proyecto es una buena práctica dividir los archivos de configuración de la siguiente forma: </p>

* main.tf: Este archivo es utilizado para poder definir todos los recursos que serán manejados por terraform. Aqui es donde podemos llamar a otros módulos de terraform para poder organizar el proyecto de mejor forma.

```
module "modulo_1" {
  source          = "./modules/modulo_1" # referencia a un submodulo
  variable_1      = var.var_1 # referencia a variables
  variable_2      = var.var_2
}
```

* variables.tf: Este archivo contiene todas las definiciones de las variables que serán utilzadas para poder configurar los recursos.

```
variable var_1 {
  type        = number
  default     = 1
  description = "variable 1"
}

variable var_2 {
  type        = list(string)
  default     = []
  description = "lista de cadenas"
}
```

* outputs.tf: define todas las salidas de un módulo. Esto es bastante conveniente al momento de usar módulos y crear dependencias entre recursos.

```
output "output" {
  value = {
    output_ejemplo = module.modulo_1
  }
}
```

#### Comandos de Terraform

Comandos que son de utilidad para terraform:

* Terraform plan: permite generar un plan de ejecución que muestra las acciones que se realizarán. Esto permite ver el panorama de los cambios que se van a ejecutar.

```bash
$ terraform plan
```

* Terraform apply: este comando permite ejecutar los cambios que se han guardado luego de ejecutar **terraform plan** 

```bash
$ terraform apply
```

* Terraform destroy: este comando permite que la infraestructura existente se pueda destruir.

```bash
$ terraform destroy
```

### Documentacion de referencia

Puedes consultar los siguientes recursos para explorar un poco mas lo que hemos hablado en este apartado:

* [Terraform Docs](https://www.terraform.io/docs/language/index.html)