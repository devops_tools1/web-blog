---
title: "Conceptos"
date: 2021-06-30T11:02:05+06:00
lastmod: 2021-06-30T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Chef","Puppet", "Conceptos"]
---

## Fundamentos

<p style='text-align: justify;'> A continuacion se presentan algunos conceptos que debemos comprender para empezar a familiarizarnos con esta herramienta de automatizacion: </p>

* Ruby: es un lenguaje de programacion utilizado para escribir cada uno de los pasos que las recetas van a ejecutar. Esto nos da la posibilidad de utilizar logica de programacion al momento de poder desarrollar pasos automatizados.

* Nodo: Es un dispositivo, fisico o virtual que estara manejado directamente por chef.

* Chef Client: es la herramienta instalada en cada uno de los nodos que nos permite ejecutar las instruciones de chef.

* Recipe: Es el equivalente a un conjunto de instrucciones que pueden ejecutarse tal y como si fueran pasos que se deben de seguir. Lo que seria equivalente a una receta de cocina.

* cookbook: Es un conjunto de recetas. Lo que se busca al utilizar cookbooks es poder tener una seria de pasos organizados en diferentes recetas para lograr configurar un nodo.


### Entendiendo la herramienta

<p style='text-align: justify;'>El concepto de la herramienta se puede entender bastante bien si lo asociamos a la razon del nombre. La analogia se basa utilizar "recetas", las cuales describen una serie de pasos y estas "recetas" (recipes) se pueden organizar en "libros de cocina" (cookbooks). </p>

<p style='text-align: justify;'> En nuestro caso, ya que deseamos implementar tareas automatizadas podemos tener un conjunto de recetass que nos pueden ser utiles para "cocinar" un servidor web, este conjunto de recetas podemos organizarlos en cookbook. En nuestro cookbook podriamos tener diferentes recipes que pueden tener los siguientes pasos:  </p>

* Actualizacion de dependencias
* Actualizacion de paquetes del servidor
* Instalacion de nginx
* Configuracion de nginx
* Despliegue del sitio web en el servidor
* Iniciar el servicio web en el servidor.

<p style='text-align: justify;'>
Para poder definir cada uno de los pasos que se deben de seguir, se hace uso de "Recursos" los cuales definen ciertas acciones que se van a a ejecutar para la configuracion del servidor y llevarlo al estado deseado. Por ejemplo:

```ruby
file '/home/user/hola_mundo.txt' do
  content 'Ejemplo de uso de recursos en chef para devops-tools!'
  mode '0755'
  owner 'user'
  group 'user'
end
```

En el apartado anterior pordemos ver un ejemplo de la utilizacion de un recurso. Este recurso de tipo "file" nos permite decirle a Chef que queremos escribir un archivo en el servidor, podemos definir los permisos, usuarios e incluso el contenido del archivo. Chef se encargara de poder revisar el estado del servidor y comprobar que el archivo se encuentre en la ruta especificada, en caso de que no sea asi, Chef, se encargara de ejecutar las acciones necesarias para que el servidor se encuentre en el estado deseado.
</p>

### Documentacion de referencia

Puedes consultar los siguientes recursos para explorar un poco mas lo que hemos hablado en este apartado:

* [Chef Overview](https://docs.chef.io/chef_overview/)
* [Chef recurso "file"](https://docs.chef.io/chef_overview/)
