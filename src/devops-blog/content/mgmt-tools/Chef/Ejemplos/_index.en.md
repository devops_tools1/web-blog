---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Chef","Ejemplos", "Docker"]
---

## Antes de empezar

Antes de poder comenzar con los ejemplos es muy importante poder contar con las siguientes herramientas instaladas:

* Docker: durante la explicacion de los ejemplos haremos el uso docker para poder tener un ambiente de pruebas aislado. En caso de que no tengas instalado docker puedes utilizar la siguiente [documentacion](https://docs.docker.com/engine/install/)

* Git: herramienta de control de versiones, puedes instalarla aqui: [documentacion](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. clonar el repositorio de ejemplos:

```bash
$ git clone https://gitlab.com/devops_tools1/iac_chef.git
```

2. ingresar al directorio del repositorio:

```bash
$ cd iac_chef 
```

3. comprobar que podemos construir la imagen:

```bash
$ docker build -t chefenv:v1 .
```

4. comprobar que podemos correr la imagen y montar el volumen para poder empezar a probar:

```bash
$ docker run -it --rm -v $PWD/cookbooks:/var/chef/cookbooks chefenv:v1 bash
```

Si todos los pasos anteriores se ejecutaron sin ningun error, puedes continuar a probar los ejemplos.

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/bIf1vreBoOA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>