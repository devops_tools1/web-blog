---
title: "Ejemplo 1: Primera receta"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Chef","Ejemplos", "Docker"]
---

### Ejemplo 1

<p style='text-align: justify;'> En este primer ejemplo buscamos presentar la funcionalidad de la herramienta, para ello vamos a presentar el siguiente ejercicio: </p>

> <p style='text-align: justify;'>  Para el primer ejemplo se necesita automatizar la tarea de utilizar Chef para poder crear las siguientes configuraciones en un servidor con el sistema operativo Ubuntu 18.04:
</p>

* Crear una carpeta en el directorio **home** con el nombre **ejemplo1**

* En la carpeta creada se debde de existir un archivo de configuracion con extension **.txt**.

* El servidor debe de tener el paquete de **git** instalado.

#### Demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7m6zj6M-5-k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>