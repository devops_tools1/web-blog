---
title: "Chef Automation"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Chef","Puppet"]
---

## ¿Qué es Chef?

<p style='text-align: justify;'> Chef es una herramienta que permite manejar la configuración de los recursos y automatizar pasos. Esta herramienta hace uso del lenguaje de programación Ruby y es ampliamente utilizada para el manejo de servidores y despliegue de aplicaciones, sean estos en servidores físicos o en la nube. Es bastante utilizada en el ámbito de DevOps para buscar que los procesos de configuración de servidores sean consistentes con el estado deseado, la herramienta no asume el estado de el nodo sino que siempre comprueba que la configuración que se está aplicando sea completada. </p>

<p style='text-align: justify;'> El nombre de la herramienta puede asociarse a cada uno de los conceptos que la componen ya que Chef utiliza un concepto de crear “recetas” o “recipes” que describen la configuración deseada y estas recetas se pueden agrupar para poder conformar un “libro de cocina” o “cookbook”, es como tener una serie de pasos que se desean ejecutar para poder llegar al estado deseado. La herramienta requiere que cada uno de los nodos en donde se ejecute la configuración tenga el cliente de Chef instalado. </p>