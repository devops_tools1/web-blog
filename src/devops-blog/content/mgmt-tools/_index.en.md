---
title: "Herramientas para el manejo de configuración"
date: 2021-07-30T11:02:05+06:00
icon: "ti-notepad"
description: "Ejemplos de herramientas para automatización de configuración"
type : "docs"
weight: 3
---

## ¿Qué son?


<p style='text-align: justify;'> Las herramientas de manejo de configuración permiten implementar un proceso para asegurar que un sistema se encuentre en un estado deseado y consistente. La importancia de estas herramientas radica en la importancia de mantener el estado y la consistencia de los sistemas, ya que en caso de que existan cambios, por muy pequeños que sean, pueden introducir problemas en ambientes de producción. El objetivo de este tipo de herramientas es poder mejorar la productividad, eficiencia y el nivel de automatización que elimine cualquier posible variante por el error humano. </p>

### ¿Qué ventajas puedo obtener?

En el ámbito DevOps utilizar este tipo de herramientas otorga ventajas, algunas de ellas son las siguientes:

* Aplicar configuraciones consistentes
* Ser eficientes al momento de exalar la infraestructura
* Documentación de las configuraciones realizadas, esto debido a que todo se encuentra en código
* Tratar la configuración como código, por lo que permite tener un historial y versión de las diferentes configuraciones realizadas en el sistema

### ¿Cómo se aplica en escenario real?

<p style='text-align: justify;'> En un ejemplo de la vida real podemos imaginar que estamos a cargo de la administración de la infraestructura que consiste en la configuración de un servidor en donde se despliega una página web sencilla. Para poder realizar la configuración necesitamos instalar todas las dependicas y usar algun tipo de servidor que permita desplegar la aplicación, imaginemos que utilizaremos un servidor como NGINX. Como en el caso hipotético estamos hablando de un solo servidor podemos asumir que los pasos que tendremos que realizar son los siguientes: </p>

1. Escoger el sistema operativo base, en nuestro caso será un sistema operativo Linux.
2. Actualizar los paquetes del sistema operativo.
3. Instalar las dependencias necesarias.
4. Instalar servidor de NGINX
5. Desplegar la aplicación en el servidor.

<p style='text-align: justify;'>
Los pasos anteriormente descritos no parecen tan complicados de realizar, en el caso de que utilicemos un solo servidor. Pero que sucede si nuestra aplicación se vuelve famosa y vemos la necesidad de poder cubrir las demanda por lo que optamos de tener un balanceador de carga enfrente de los servidores, pero esto implica que debemos de configurar cada uno de ellos de la misma forma lo cual puede ser bastante trabajoso e incluso puede incurrir en errores de configuraciones como por ejemplo, tener uno o dos servidores que no tienen la misma configuración, en estos casos es donde las herramientas que permiten automatizar estos procesos se vuelven de mucha ayuda. Más adelante haremos mención de algunos ejemplos de este tipo de herramientas.
 </p>