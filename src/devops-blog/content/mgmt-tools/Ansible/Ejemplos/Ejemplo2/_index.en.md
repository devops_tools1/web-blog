---
title: "Ejemplo 2: Playbooks y variables con Ansible"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Ansible","Ejemplos", "Docker"]
---

### Ejemplo 2

<p style='text-align: justify;'> En este segundo ejemplo buscamos presentar la funcionalidad de la herramienta, para ello vamos a presentar el siguiente ejercicio: </p>

> <p style='text-align: justify;'>  Se necesita automatizar la tarea de utilizar Ansible para poder crear las siguientes configuraciones en un servidor con el sistema operativo Ubuntu 18.04:
</p>

1. Crear una carpeta en el directorio **home** utilizando atributos pasados al servidor, es decir que se deben parametrizar: nombre y permisos de la carpeta

2. Se debe de crear un archivo en **home** con configuraciones, utilizando variables, se deben parametrizar: el nombre, los permisos y el contenido del archivo

3. Se debe de crear un archivo estatico hacia la carpeta **home** del servidor, esto utilizando el directorio **files** que ofrece Ansible


4. Instalar todos los paquetes que se especifiquen por medio de un arreglo en al archivo de variables de Ansible, estos pueden ser uno o mas paquetes que se necesiten instalar en el servidor.

#### Demo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Fahnef1AJG4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>