---
title: "Ejemplos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Ansible","Ejemplos", "Docker"]
---

## Antes de empezar

<p style='text-align: justify;'> Para los siguientes ejemplos estan pensados para ser probados utilizando un contenedor de Docker, por lo que se provee el dockerfile necesario para poder instalar dependencias y todo lo que se necesita para poder correr y probar cada uno de los ejemplo. Si quieres mayor informacion de como instalar docker en tu computadora local puedes dirigirte al siguiente enlace: </p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

* Git: herramienta de control de versiones, puedes instalarla aqui: [documentacion](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Preparando el ambiente:

Para poder tener el ambiente de pruebas listo:

1. clonar el repositorio de ejemplos:

```bash
$ git clone https://gitlab.com/devops_tools1/iac_ansible.git
```

2. ingresar al directorio del repositorio:

```bash
$ cd iac_ansible
```

3. comprobar que podemos construir la imagen:

```bash
$ docker build -t <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> .
```

4. comprobar que podemos correr la imagen y montar el volumen para poder empezar a probar:

```bash
$ docker run -it --rm -v $PWD/playbooks:/var/ansible/playbooks <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> bash
```

Si todos los pasos anteriores se ejecutaron sin ningun error, puedes continuar a probar los ejemplos.

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/lRn5fQgs-dg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>