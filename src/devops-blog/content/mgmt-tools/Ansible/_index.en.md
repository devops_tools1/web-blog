---
title: "Ansible"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Ansible"]
---

## ¿Qué es Ansible?

<p style='text-align: justify;'> Ansible es un herramienta orientada a la automatización de procesos. Es ampliamente utilizada por profesionales en el ámbito de la tecnología de la información y DevOps. Las diferentes aplicaciones de esta herramienta van desde la automatización de tareas, desplegar una aplicación, realizar el manejo de la configuración hasta poder provisionar recursos en la nube. La ventaja de Ansible como herramienta es que no se requiere conocimientos en programación. </p>

<p style='text-align: justify;'> Esta herramienta tiene un flujo de trabajo bastante sencillo, utiliza pequeños módulos que son ejecutados nodos remotos, cada uno de estos módulos realizan pasos para configurar cada uno de los nodos. Una de las ventajas de esta herramienta es que no necesita tener agentes instalados en los nodos a los que se quiere aplicar una configuración en lugar de ello utiliza el protocolo de ssh para ejecutar cada una de las tareas. La herramienta utiliza el lenguaje conocido como YAML para definir cada una de las tareas que buscan llevar un nodo a un estado deseado. </p>