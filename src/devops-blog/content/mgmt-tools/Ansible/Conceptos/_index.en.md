---
title: "Conceptos"
date: 2021-01-30T11:02:05+06:00
lastmod: 2021-01-30T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["iac", "aws", "infraestructura", "configuración", "Ansible"]
---

## Fundamentos

<p style='text-align: justify;'> A continuacion se presentan algunos conceptos que debemos comprender para empezar a familiarizarnos con esta herramienta de automatizacion: </p>

* Nodo de Control: es el nodo de control que permite ejecutar comandos de **ansible** para el control de nodos múltiples.

* Nodos manejados: servidores en la red que pueden ser manejados con **ansible**.

* Inventario: Archivo de configuración que permite tener un listado de nodos que son manejados por **ansible**.

* Modulos: Unidades de código que permiten definir ciertas acciones en **ansible**.

* Tareas: acciones que **ansible** ejecuta para llegar a configurar los nodos.

* Playbooks: Es un conjunto de tareas que pueden organizarse para ejecutar una configuración.

### Entendiendo la herramienta

<p style='text-align: justify;'>
La herramienta trabaja de forma bastante sencilla, esta se conecta a los nodos que se definen por medio del inventario ejecutando cada una de las tareas que son llamadas "modulos de ansible". Esta herramienta no necesita tener un agente instalado en el nodo destino, ya que utiliza la conexión de tipo ssh para poder ejecutar las tareas de configuración.
</p>

<p style='text-align: justify;'>
Esta herramienta es muy utilizada para automatizar cada una de las tareas que deben ejecutarse en un servidor, con el fin de llevar ese recurso a un estado deseado. Esta herramienta es ejecutada utilizando el lenguaje de programación Python, pero todas las configuraciones son descritas utilizando archivos YAML, los cuales permiten tener una visión clara de cada uno de los pasos que se desean ejecutar
</p>

<p style='text-align: justify;'> Ansible utiliza un archivo llamado inventario, donde podemos describir conjunto de servidores y organizarlos de acuerdo a lo que necesitamos:</p>

```config
[servidores_db]
db1.internal.com
db2.internal.com

[servidores_web]
webapp1.internal.com
webapp2.internal.com
```

<p style='text-align: justify;'>
Utilizando ansible playbooks podemos organizar cada una de las configuraciones que deseamos ejecutar para configurar cada tipo de servidores, por ejemplo:
</p>

Servidores web:

* Actualizar dependencias

* Instalar nginx como servidor web

* Configurar el directorio de nuestra aplicación

* Clonar la aplicación en el directorio raíz.

* Asegurarnos que el proceso del servidor y puertos esten disponibles.

Servidores de base de datos:

* Instalar el motor de base de datos

* Configurar el motor de base de datos.

* Iniciar los puertos para poder servir la base de datos

Estas tareas podemos automatizarlas describiendo que queremos realizar en cada uno de los servidores.

#### Tareas de ansible

<p style='text-align: justify;'> Para llevar a cabo la configuración podemos escribir "playbooks" que permitan definir los pasos que deseamos ejecutar en el nodo destino: </p>

Servidores web:

```yaml
---
- name: Instalando servidores web
  hosts: servidores_web # nodos destinos del archivo de configuracion
  remote_user: web_user # usuario de ssh a utilizars

  tasks:
    - name: Ensure apache is at the latest version
      # modulo de ansible: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html
      apt:
        name: nginx
        state: latest
        state: present

    ... # mas tareas a realizar...

```

Servidores de db:

```yaml
---
- name: Instalando servidores web
  hosts: servidores_db # nodos destinos del archivo de configuracion
  remote_user: db_user # usuario de ssh a utilizars

  tasks:
    - name: Ensure apache is at the latest version
      # modulo de ansible: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html
      apt:
        name: mysql
        state: latest
        state: present

    ... # mas tareas a realizar...

```

<p style='text-align: justify;'>  Ansible se encargara de ejecutar las acciones en los nodos destino, utilizando la configuración de SSH especificada.</p>