# Web Blog

> Proyecto que contiene la definicion de el blog web para presentar los ejemplos

# Antes de empezar

Este proyecto utiliza las siguientes herramientas:

* Docker: herramienta de contenedores, la **CLI** utilizada para generar el sitio estático es llamado **Hugo** y la herramienta esta siendo utilizada con un contenedor oficial que se puede encontrar [aqui](https://hub.docker.com/r/klakegg/hugo/)

* Hugo Static Generator: herramienta para genera sitios web estaticos [Documentacion oficial de la herramienta](https://gohugo.io/)

* Tema del sitio: Hugo permite que se integren temas que pueden descargarse, en el caso de este blog se esta utilizando el siguiente tema: [Guthub del tema](https://github.com/jeblister/kube)

**IMPORTANTE**: debe de tener instalado Docker en su ordenador local para funcionar correctamente y probar el proyecto


# Desarrollo Local

Para probar localmente el siguiente proyecto:

1. Construir la imagen de docker:
```bash
$ docker build -t hugocli:cli ./docker
```

2. Luego de construir la imagen puedes ejecutar los comandos de la cli de **hugo** de la siguiente forma:
```bash
$ docker run --rm -v $PWD/src/devops-blog:/src -p 1313:1313 hugocli:cli --help
```

obtendras el siguiente output:
```bash
hugo is the main command, used to build your Hugo site.

Hugo is a Fast and Flexible Static Site Generator
built with love by spf13 and friends in Go.

Complete documentation is available at http://gohugo.io/.

Usage:
  hugo [flags]
  hugo [command]

Available Commands:
  config      Print the site configuration
  convert     Convert your content to different formats
  deploy      Deploy your site to a Cloud provider.
  env         Print Hugo version and environment info
  gen         A collection of several useful generators.
  help        Help about any command
  import      Import your site from others.
  list        Listing out various types of content
  mod         Various Hugo Modules helpers.
  new         Create new content for your site
  server      A high performance webserver
  version     Print the version number of Hugo

Flags:
  -b, --baseURL string             hostname (and path) to the root, e.g. http://spf13.com/
  -D, --buildDrafts                include content marked as draft
  -E, --buildExpired               include expired content
  -F, --buildFuture                include content with publishdate in the future
      --cacheDir string            filesystem path to cache directory. Defaults: $TMPDIR/hugo_cache/
      --cleanDestinationDir        remove files from destination not found in static directories
      --config string              config file (default is path/config.yaml|json|toml)
      --configDir string           config dir (default "config")
  -c, --contentDir string          filesystem path to content directory
      --debug                      debug output
  -d, --destination string         filesystem path to write files to
      --disableKinds strings       disable different kind of pages (home, RSS etc.)
      --enableGitInfo              add Git revision, date and author info to the pages
  -e, --environment string         build environment
      --forceSyncStatic            copy all files when static is changed.
      --gc                         enable to run some cleanup tasks (remove unused cache files) after the build
  -h, --help                       help for hugo
      --i18n-warnings              print missing translations
      --ignoreCache                ignores the cache directory
      --ignoreVendor               ignores any _vendor directory
      --ignoreVendorPaths string   ignores any _vendor for module paths matching the given Glob pattern
  -l, --layoutDir string           filesystem path to layout directory
      --log                        enable Logging
      --logFile string             log File path (if set, logging enabled automatically)
      --minify                     minify any supported output format (HTML, XML etc.)
      --noChmod                    don't sync permission mode of files
      --noTimes                    don't sync modification time of files
      --path-warnings              print warnings on duplicate target paths etc.
      --print-mem                  print memory usage to screen at intervals
      --quiet                      build in quiet mode
      --renderToMemory             render to memory (only useful for benchmark testing)
  -s, --source string              filesystem path to read files relative from
      --templateMetrics            display metrics about template executions
      --templateMetricsHints       calculate some improvement hints when combined with --templateMetrics
  -t, --theme strings              themes to use (located in /themes/THEMENAME/)
      --themesDir string           filesystem path to themes directory
      --trace file                 write trace to file (not useful in general)
  -v, --verbose                    verbose output
      --verboseLog                 verbose logging
  -w, --watch                      watch filesystem for changes and recreate as needed

Additional help topics:
  hugo check   Contains some verification checks

Use "hugo [command] --help" for more information about a command.
```

3. Para ver el sitio de forma local:
```bash
$ docker run --rm -v $PWD/src/devops-blog:/src -p 1313:1313 hugocli:cli serve -w
```
podras ver el sitio funcionando en http://localhost:1313

## Usando el script

Se ha provisto de un script para correr de forma local los pasos anteriores, es compatible en Linux y Mac (script de bash), puedes ejecutarlo de la siguiente forma:

ejemplos:

ver el help de la cli
```bash
$ ./hugo --help
```

correr el server web local:
```bash
$ ./hugo serve -w
```